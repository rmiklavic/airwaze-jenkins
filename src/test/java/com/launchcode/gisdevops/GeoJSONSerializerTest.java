package com.launchcode.gisdevops;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.launchcode.gisdevops.features.Feature;
import com.launchcode.gisdevops.features.FeatureCollection;
import com.launchcode.gisdevops.features.WktHelper;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class GeoJSONSerializerTest {

    private SerializerProvider provider;

    @Test
    public void Test() {
    }

    @Test
    public void shouldSerializeProperly() throws JsonProcessingException {

        FeatureCollection featureCollection = new FeatureCollection();
        Map<String, Object> properties = new HashMap<>();
        properties.put("airline", "ZL");
        properties.put("src", "PLO");
        properties.put("dst", "ADL");
        featureCollection.addFeature(new Feature(WktHelper.wktToGeometry("LINESTRING(135.880004883 -34.6053009033,138.531005859375 -34.94499969482422)"), properties));

        String actual = new ObjectMapper().writeValueAsString(featureCollection);

        Assert.assertThat(actual, CoreMatchers.containsString("{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"geometry\":{\"type\":\"LineString\",\"coordinates\":[[135.880004883,-34.6053009033],[138.531005859375,-34.94499969482422]]},\"properties\":{"));
    }
}
