package com.launchcode.gisdevops.models;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Route implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Geometry routeGeom;

    private String airline;
    private Integer airlineId;
    private String src;
    private Integer srcId;
    private String dst;
    private Integer dstId;

    protected Route() {
    }

    public Route(Geometry routeGeom, String airline, Integer airlineId, String src, Integer srcId, String dst, Integer dstId) {
        this.routeGeom = routeGeom;
        this.airline = airline;
        this.airlineId = airlineId;
        this.src = src;
        this.srcId = srcId;
        this.dst = dst;
        this.dstId = dstId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Geometry getRouteGeom() {
        return routeGeom;
    }

    public void setRouteGeom(Point routeGeom) {
        this.routeGeom = routeGeom;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public Integer getAirlineId() {
        return airlineId;
    }

    public void setAirlineId(Integer airlineId) {
        this.airlineId = airlineId;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public Integer getSrcId() {
        return srcId;
    }

    public void setSrcId(Integer srcId) {
        this.srcId = srcId;
    }

    public String getDst() {
        return dst;
    }

    public void setDst(String dst) {
        this.dst = dst;
    }

    public Integer getDstId() {
        return dstId;
    }

    public void setDstId(Integer dstId) {
        this.dstId = dstId;
    }

}
