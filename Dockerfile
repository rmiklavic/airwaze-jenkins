FROM kartoza/postgis:9.4-2.1
ENV POSTGRES_USER=airwaze_user
ENV POSTGRES_PASS=airwazepass
ENV ALLOW_IP_RANGE=<0.0.0.0/0>
COPY Airports.csv /tmp
COPY routes.csv /tmp
