package com.launchcode.gisdevops.data;

import com.launchcode.gisdevops.models.Airport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface AirportRepository extends JpaRepository<Airport, Integer>{

    public List<Airport> findByFaaCode(String faaCode);
}
